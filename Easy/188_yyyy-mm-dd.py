# yyyy-mm-dd
# mm/dd/yy
# mm#yy#dd
# dd*mm*yyyy
# (month word) dd, yy
# (month word) dd, yyyy

from time import strptime

date_formats = ['%Y-%m-%d', '%m/%d/%y', '%m#%y#%d', 
                '%d*%m*%Y', '%b %d, %y', '%b %d, %Y']

with open('188_input_dates.txt', 'r') as f:
    for line in f:
        for df in date_formats:
            try:
                a = strptime(line.rstrip(), df)
                print('{}-{}-{}'.format(a.tm_year, a.tm_mon, a.tm_mday))
                break
            except ValueError:
                pass
