def custom_justify(txt, spaces, right=True):
    """
    :param txt: String to justify
    :param spaces: Integer of spaces to adjust justification
    :param right: Right or Left justify boolean
    :return: String that's been justified as per given values
    """
    if right is not False and right is not True:
        right = True                    # defaults to right justify
    l_r = '>' if right else '<'
    return '{0: {1}{2}}'.format(txt.strip('0'), l_r, spaces)


def carry_addition(nums, top=True):
    """
    :param nums: List of integers, each integer as a string
    :param top: If true, carried numbers on top. Otherwise, bottom (UK?)
    :return: Prints the result
    """
    if top is not False and top is not True:
        top = True                      # defaults to carried numbers on top

    total = sum(int(a) for a in nums)   # sum of nums
    long = max(len(b) for b in nums)    # length of longest integer
    nums_2 = [c if len(c) == long else  # adjust each number to be same length
              ''.join(((long - len(c)) * '0', c)) for c in nums]
    columns = [sum(int(e) for e in d) for d in zip(*nums_2)]  # sum each column

    last = 0                            # first column always starts at 0
    top_row = list()
    for f in columns[::-1]:             # reversed, goes from right to left
        g = divmod(f + last, 10)[0]     # calculate sum carried over
        top_row.append(str(g) if g >= 1 else '0')
        last = g                        # add to the next column

    carry = '{}'.format(''.join(top_row[::-1]).strip('0'))
    c_len = sum(1 for h in carry if h.isdigit())  # count digits, not spaces
    long = long + 1 if c_len == long else long    # adjust justification value
    if top:
        print(carry)
    [print('{}'.format(custom_justify(h, long))) for h in nums_2]
    print('{}\n{}'.format('-' * long, custom_justify(str(total), long)))
    if not top:
        print('-' * long)
        print(carry)
    print()

tests = '''\
559+447+13+102
23+9+66
23+23+24'''.split('\n')

for test in tests:
    carry_addition(test.rstrip().split('+'))         # carry on TOP
    carry_addition(test.rstrip().split('+'), False)  # carry on BOTTOM

# Bonus: Extend your program to handle non-integer (ie. decimal numbers)


# # u/13467 - reddit *** Way less lines than mine!! Similar, but BETTER!!!
# from itertools import zip_longest
#
# terms = input().split('+')
# total = sum(map(int, terms))
# width = len(str(total))
#
# print('\n'.join(t.rjust(width) for t in terms))
# print('-' * width)
# print(str(total).rjust(width))
#
# carry, c = ' ', 0
# for digits in zip_longest(*map(reversed, terms), fillvalue='0'):
#     c = (sum(map(int, digits)) + c) // 10
#     carry = (str(c) if c > 0 else ' ') + carry
#
# print('-' * width)
# print(carry[-width:])
