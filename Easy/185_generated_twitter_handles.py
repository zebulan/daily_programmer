def twitter_handles(wordlist, bonus=False):
    """
    :param wordlist: Text file of words, one per line.
    :param bonus: If bonus is True, find words that have 'at' in them at any
                  point of the word and replace it with the @ symbol.
                  Otherwise, find words starting with 'at', replace with '@'.
    :return: Print out the 10 shortest and 10 longest words that
             that match the criteria described above.
    """
    with open(wordlist, 'r') as f:
        all_words = dict()
        for line in f:
            word = line.rstrip()
            if bonus and 'at' in word:
                all_words[word.replace('at', '@')] = word
            elif word.startswith('at'):
                all_words[word.replace('at', '@', 1)] = word

        sort_words = sorted(all_words, key=len)
        print('>> Shortest <<')
        [print('{} : {}'.format(b, all_words[b])) for b in sort_words[:10]]
        print('\n>>Longest<<')
        [print('{} : {}'.format(c, all_words[c])) for c in sort_words[-10:]]
        print()

twitter_handles('185_enable1.txt')
twitter_handles('185_WordList.txt', True)
