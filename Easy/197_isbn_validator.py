def isbn_sum(dig_list):
    return sum(b * c for b, c in zip(range(10, 0, -1), dig_list))


def isbn_validator(isbn):
    digits = [int(a) for a in isbn if a.isdigit()]
    last_x = True if isbn[-1].lower() == 'x' else False
    if len(digits) == 10 and not last_x:  # normal single digits only
        return isbn_sum(digits) % 11 == 0
    elif len(digits) == 9 and last_x:     # add 10 for 'X'
        return (isbn_sum(digits) + 10) % 11 == 0
    return False                          # wrong number of digits


assert isbn_validator('0-7475-3269-9') is True
assert isbn_validator('0-306-40615-2') is True
assert isbn_validator('156881111X') is True
assert isbn_validator('1568811112X') is False

# submitted, no response yet.
