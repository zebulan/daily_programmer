f = """\
2015-07-01 2015-07-04
2015-12-01 2016-02-03
2015-12-01 2017-02-03
2016-03-01 2016-05-05
2017-01-01 2017-01-01
2022-09-05 2023-09-04""".split('\n')

"""\
July 1st - 4th
December 1st - February 3rd
December 1st, 2015 - February 3rd, 2017
March 1st - May 5th, 2016
January 1st, 2017
September 5th, 2022 - September 4th, 2023
"""
from datetime import datetime

for line in f:
    a, b = (datetime.strptime(c, '%Y-%m-%d').timetuple()
            for c in line.rstrip().split())

    if a == b:  # same date
        print(a.tm_year, a.tm_mon, a.tm_mday)
        # print(a.strftime('%B %d, %Y'))
        break
    yy = a.tm_year == b.tm_year
    mm = a.tm_mon == b.tm_mon
    dd = a.tm_yday == b.tm_year

    print(yy, mm, dd)
    if yy:  # years are the same, use single year on the right
        pass
        # print('{} {}, {} - {} {}, {}'.format())
    else:  # years are different, use two years. left & right
        pass

    if mm and yy: # <-- months are the same and ?
        pass  # if same month and same year, use single month on left

    else:  # if same month but different year, use both months. left & right
        pass  # always use both months




    # a, b = [[int(c) for c in date.split('-')] for date in line.rstrip().split()]
    # print(a, b)
    # y, m, d = a
    # y2, m2, d2 = b
    # print(y, m, d)
    # print(y2, m2, d2)


