from collections import Counter
from string import punctuation


def no_punc(txt):
    txt = ''.join(a if a not in punctuation else ' ' for a in txt).lower()
    # txt = ''.join(a for a in txt if a not in punctuation).lower()
    return [txt] if txt.count(' ') == 0 else txt.split()

punctuation = punctuation.replace('\'', '')

with open('count_of_monte_cristo.txt', 'r') as f:
    monte_cristo = list()
    for b in f.readlines():  # separate the lines
        if b.startswith('Produced by'):
            continue
        elif b.startswith('End of Project Gutenberg\'s'):
            break
        for c in b.strip().split():  # words in each line
            for d in no_punc(c):     # in case of multiple words
                monte_cristo.append(d.strip('\''))

cnt = Counter(monte_cristo)
print(cnt.most_common(10))
