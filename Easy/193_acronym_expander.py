acronym = {
    'lol': 'laugh out loud',
    'dw': 'don\'t worry',
    'hf': 'have fun',
    'gg': 'good game',
    'brb': 'be right back',
    'g2g': 'got to go',
    'wtf': 'what the fuck',
    'wp': 'well played',
    'gl': 'good luck',
    'imo': 'in my opinion'}


def expander(txt):
    return ' '.join(scrub_word(a) for a in txt.rstrip().split())


def scrub_word(txt):
    if txt.isalpha():            # without punctuation
        if txt in acronym:
            return acronym[txt]  # matching acronym
        return txt               # no matching acronym, return as is

    chunks = list()              # split the 'word' into chunks
    alpha = list()               # chunks that are alpha-numeric
    punc = list()                # chunks that are NOT alpha-numeric
    for char in txt:
        if char.isalnum():
            if punc:
                chunks.append(''.join(punc))
                punc = list()
            alpha.append(char)
        else:
            if alpha:
                chunks.append(''.join(alpha))
                alpha = list()
            punc.append(char)
    # whichever is the last one
    if punc:
        chunks.append(''.join(punc))
    elif alpha:
        chunks.append(''.join(alpha))
    return ''.join(b if b not in acronym else acronym[b] for b in chunks)

assert expander('wtf that was unfair') == 'what the fuck that was unfair'
assert expander('gl all hf') == 'good luck all have fun'
assert expander('imo that was wp. Anyway I\'ve g2g') == \
       'in my opinion that was well played. Anyway I\'ve got to go'
assert expander('imo that was ...wp. Anyway I\'ve ,,g2g.') == \
       'in my opinion that was ...well played. Anyway I\'ve ,,got to go.'
