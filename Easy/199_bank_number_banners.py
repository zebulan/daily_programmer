def big_dict():
    """ Return a dictionary of BIG font digits. Numbers are 5x6 grids. """
    nums = '''\
 _     _  _     _  _  _  _  _ 
| |  | _| _||_||_ |_   ||_||_|
|_|  ||_  _|  | _||_|  ||_| _|'''
    big = {}                            # create dictionary for 'BIG' font
    for x in range(0, 10):              # set keys 0-9
        big[str(x)] = {}                # set values as empty dict

    s_key = 0                           # secondary key counter
    for x in nums.split('\n'):          # split each line by newline character
        p_key = 0                       # primary key counter
        for y in [x[z:z+3] for z in range(0, len(x), 3)]:  # 3 character chunks
            big[str(p_key)][s_key] = y  # set line value as 3 character chunk
            p_key += 1                  # number counter
        s_key += 1                      # line counter
    return big                          # return dictionary


def make_big(txt):
    fix = ''.join([x for x in txt if x.isdigit()])  # ignore all but digits
    [print(z) for z in [''.join([b[str(y)][x] for y in fix]) for x in range(3)]]

b = big_dict()
[make_big(line.rstrip()) for line in ['000000000', '111111111', '490067715']]
