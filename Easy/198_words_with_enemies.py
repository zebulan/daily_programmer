from collections import Counter


def left_over(C_dict):
    return ''.join(letter * C_dict[letter] for letter in C_dict)

def rank_both(one, two):
    left = left_over(one - two)
    right = left_over(two - one)
    l, r = len(left), len(right)
    return left, right, "Tie!" if l == r else \
        ("Left Wins!" if l > r else "Right Wins!")

tests = '''\
because cause
hello below
hit miss
rekt pwn
combo jumbo
critical optical
isoenzyme apoenzyme
tribesman brainstem
blames nimble
yakuza wizard
longbow blowup'''.split('\n')

for test in tests:
    curr = test.rstrip()
    if curr:
        l, r = curr.split()
        print('{} | {}\n{}\n'.format(*rank_both(Counter(l), Counter(r))))
