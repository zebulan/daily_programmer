from bs4 import BeautifulSoup
from requests import get


def neg_pos(filename):
    with open(filename, 'r') as f:
        tmp = set()
        for line in f:
            curr = line.rstrip()
            if not curr.startswith(';') and curr:
                tmp.add(curr)
    return tmp


def score_comment(txt):
    txt = ''.join(a for a in txt if a.isalnum() or a.isspace()).lower().split()
    unique = set(txt)
    h_total = sum(txt.count(b) for b in unique if b in positive)
    s_total = sum(txt.count(c) for c in unique if c in negative)
    return ' '.join(txt), h_total, s_total


url = 'https://plus.googleapis.com/u/0/_/widget/render/comments?first_party_property=YOUTUBE&href='
youtube = input('Enter YouTube URL: ')
r = get(url + youtube)
soup = BeautifulSoup(r.text)

negative = neg_pos('negative-words.txt')
positive = neg_pos('positive-words.txt')
total_comments = 0
print('-' * 10)

for div in soup.find_all('div', class_='Ct'):
    # skip username on replies by grabbing last string only
    comment, h, s = score_comment(div.contents[-1])  
    if comment:
        print(comment)
        if h == s:
            print('This comment is equally happy/sad ({}/{})'.format(h, s))
        elif h > s:
            print('This comment is mostly happy ({}/{})'.format(h, s))
        else:
            print('This comment is mostly sad ({}/{})'.format(s, h))
        print('-' * 10)
        total_comments += 1

print('Sample size: {} comments\n'.format(total_comments))
